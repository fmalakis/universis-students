FROM node:lts-buster AS builder

# create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
# copies package.json and package lock to
# take advantage of layering in image building
COPY package.json package-lock.json /usr/src/app/
RUN npm ci

COPY . /usr/src/app
RUN npm run build


FROM nginx:stable-alpine AS production
COPY --from=builder /usr/src/app/dist /usr/share/nginx/html

EXPOSE 80
