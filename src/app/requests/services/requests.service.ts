import { Injectable, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import { ConfigurationService } from '@universis/common';
import { RequestTypesService, RequestTypeItem as RequestTypeItemBase } from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';

interface RequestTypeItem extends RequestTypeItemBase {
  additionalType?: string;
}

@Injectable()
export class RequestsService {

  private isInitialized = false;

  constructor(
    private _context: AngularDataContext,
    private _configuration: ConfigurationService,
    private _requestTypesService: RequestTypesService,
    private _translateService: TranslateService,
    private _http: HttpClient
    ) { }

  /**
   *
   * Retrieves the available request types
   *
   */
  async getRequestTypes(): Promise<Array<RequestTypeItem>>  {

    if (!this.isInitialized) {
      await this.populateRequestTypes();
      this.isInitialized = true;
    }

    return this._requestTypesService.getItems();
  }

  /**
   *
   * Reads the document types
   *
   */
  getDocumentTypes() {
    return this._context.model('DocumentConfigurations')
      .asQueryable()
      .where('inLanguage')
      .equal(this._configuration.currentLocale)
      .take(-1)
      .getItems();

  }

  /**
   *
   * Adds one or more items at the request types service.
   *
   * @param {Array<RequestTypeItem>} items The list of the request types items
   *
   */
  addRequestTypes(...items: Array<RequestTypeItem>) {
    this._requestTypesService.addRange(...items);
  }

  getActiveDocumentRequests() {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .where('actionStatus/alternateName')
      .equal('ActiveActionStatus')
      .expand('object', 'actionStatus, messages($orderby=dateCreated desc;$expand=attachments, action($expand=actionStatus))')
      .orderByDescending('dateCreated')
      .take (-1)
      .getItems();
  }

  getStudentRequests() {
    return this._context.model('StudentRequestActions')
      .asQueryable()
      .expand('actionStatus, messages($orderby=dateCreated desc;$expand=attachments, action($expand=actionStatus))')
      .orderByDescending('dateCreated')
      .take (-1)
      .getItems();
  }

  getDocumentRequests() {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .expand('object', 'actionStatus, messages($orderby=dateCreated desc;$expand=attachments, action($expand=actionStatus)')
      .orderByDescending('dateCreated')
      .take (-1)
      .getItems();
  }

  getMessageRequests() {
    return this._context.model('RequestMessageActions')
      .asQueryable()
      .expand('actionStatus, messages($orderby=dateCreated desc;$expand=attachments, action($expand=actionStatus))')
      .orderByDescending('dateCreated')
      .take (-1)
      .getItems();
  }

  getDocumentRequest(id) {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .where('id')
      .equal(id)
      .expand('object', 'actionStatus, messages($orderby=dateCreated desc;$expand=attachments, action($expand=actionStatus))')
      .getItem();
  }



  downloadFile(attachments) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachments[0].url.replace(/\\/g, '/').replace('/api', '');

    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {

        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachments[0].name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

    /**
   *
   * Add the document types and the default request
   *
   */
  async populateRequestTypes() {

    let documentTypes = await this.getDocumentTypes();
    // filter only success validation result
    documentTypes = documentTypes.filter (documentType => {
        return documentType.validationResult && documentType.validationResult.success === true;
      });
    let documentRequestTypeItems = documentTypes.map((item) => {
      const requestTypeItem: RequestTypeItem = {
        name: item.name,
        alternateName: item.alternateName,
        category: 'Requests.Certificates',
        entryPoint: item.url || 'requests/new',
        description: item.description,
        additionalType: item.additionalType
      };

      return requestTypeItem;
    });
      let requestConfigurations = await this.getStudentRequestConfigurations();
      // get only available requests
      requestConfigurations = requestConfigurations.filter(requestType => {
        return requestType.validationResult && requestType.validationResult.success === true;
      });
      requestConfigurations = requestConfigurations.map((item) => {
        const requestTypeItem: RequestTypeItem = {
          name: item.name,
          alternateName: item.alternateName,
          category: item.category ? item.category.name : 'Requests.Requests',
          entryPoint: item.url,
          description : item.description,
          additionalType: item.additionalType
        };
        return requestTypeItem;
      });
    documentRequestTypeItems = [...documentRequestTypeItems, ...requestConfigurations];
    this.addRequestTypes(...documentRequestTypeItems);

      const studentRequestTypes = {
        name: this._translateService.instant('NewRequestTemplates.OtherRequest.Name'),
        alternateName: 'OtherRequest',
        category: 'Requests.Requests',
        entryPoint: `requests/new`
      };
      this.addRequestTypes(studentRequestTypes);
  }


  async getStudentRequestConfigurations() {
    return this._context.model('StudentRequestConfigurations')
      .asQueryable()
      .where('inLanguage')
      .equal(this._configuration.currentLocale)
      .expand('category')
      .getItems();
  }

  getReportTemplates(): any {
    // get available report templates
    return this._context.model(`/students/me/reports`)
      .asQueryable()
      .expand('reportCategory')
      .getItems();
  }

  /**
   * Prints a report template
   * @param {number} id
   * @param {*} reportParams
   * @param {string} downloadName
   */
  async printReport(id: number, reportParams: any, downloadName: string){
    const headers = new Headers({
      'Accept': 'application/pdf',
      'Content-Type': 'application/json'
    });
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // get print url
    const printURL = this._context.getService().resolve(`students/me/reports/${id}/print`);
    return this._http.post(printURL, reportParams, {
      headers: this._context.getService().getHeaders(),
      responseType: 'blob',
      observe: 'response'
    }).toPromise().then( (response) => {
      const contentLocation = response.headers.get('content-location');
      if (contentLocation != null) {
        Object.defineProperty(response.body, 'contentLocation', {
          configurable: true,
          enumerable: true,
          writable: true,
          value: contentLocation
        });
      }
      return response.body;
    }).then(res => {
      const objectUrl = window.URL.createObjectURL(res);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = objectUrl;
      const name = `${downloadName}.pdf`;
      a.download = name;
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(res, name );
      } else {
        a.click();
      }
      window.URL.revokeObjectURL(objectUrl);
      a.remove(); // remove the element
    });
  }
}
